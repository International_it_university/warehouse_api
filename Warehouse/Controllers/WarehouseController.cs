﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Models.WarehouseModels;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseController : BaseController
    {
        EFGenericRepository<TblWarehouse> _repo = new EFGenericRepository<TblWarehouse>(dbCntx);

        [HttpGet]
        public IEnumerable<Object> Get()
        {
            return _repo.GetList();
        }

        // GET: api/method/5
        [HttpGet("{id}")]
        public Object Get(int id)
        {
            return _repo.GetItemById(id);
        }

        // POST: api/method
        [HttpPost]
        public void Post([FromBody] WarehouseModel value)
        {
            try
            {
                _repo.Create(new TblWarehouse { Name = value.Name, Description = value.Description });
            }
            catch (Exception ex)
            {
                throw new Exception("ошибка при сохранении");
            }
        }

        // PUT: api/method/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] WarehouseModel value)
        {
            try
            {
                var item = _repo.GetItemById(id);
                item.Name = value.Name;
                item.Description = value.Description;

                _repo.Update(item);
            }
            catch (Exception ex)
            {
                throw new Exception("ошибка при обновлении: " + ex.Message);
            }
        }

        // DELETE: api/method/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                var item = _repo.GetItemById(id);
                if (item == null) throw new Exception("удаляемый элемент не найден");

                _repo.Remove(item);
            }
            catch (Exception ex) { throw new Exception("ошибка при удалении: " + ex.Message); }
        }
    }
}
