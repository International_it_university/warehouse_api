﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Models.WarehouseModels;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShelfController : BaseController
    {
        EFGenericRepository<TblShelf> _repo = new EFGenericRepository<TblShelf>(dbCntx);

        [HttpGet]
        public IEnumerable<Object> Get()
        {
            return _repo.GetList();
        }

        // GET: api/method/5
        [HttpGet("{id}")]
        public Object Get(int id)
        {
            return _repo.GetItemById(id);
        }

        // POST: api/method
        [HttpPost]
        public void Post([FromBody] ShelfModel value)
        {
            try
            {
                _repo.Create(new TblShelf
                {
                    ContainerId = value.ContainerID,
                    TypeId = value.TypeID,
                    Xaxis = value.Xaxis,
                    Yaxis = value.Yaxis,
                    Zaxis = value.Zaxis,
                    FreeSpace = value.Xaxis * value.Yaxis * value.Zaxis
                });
            }
            catch (Exception ex)
            {
                throw new Exception("ошибка при сохранении: " + ex.Message);
            }
        }

        // PUT: api/method/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ShelfModel value)
        {
            try
            {
                var item = _repo.GetItemById(id);
                item.TypeId = value.TypeID;
                item.ContainerId = value.ContainerID;
                item.Xaxis = value.Xaxis;
                item.Yaxis = value.Yaxis;
                item.Zaxis = value.Zaxis;
                item.FreeSpace = value.Xaxis * value.Yaxis * value.Zaxis;

                _repo.Update(item);
            }
            catch (Exception ex)
            {
                throw new Exception("ошибка при обновлении: " + ex.Message);
            }
        }

        // DELETE: api/method/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var item = _repo.GetItemById(id);
            if (item == null) throw new Exception("удаляемый элемент не найден");
            try
            {
                _repo.Remove(item);
            }
            catch (Exception ex) { throw new Exception("ошибка при удалении: " + ex.Message); }
        }
    }
}
