﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Serivce.db;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;
using Warehouse.Serivce.Services.Shelf.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShelfsController : ControllerBase
    {
        EFGenericRepository<WarehouseShelfs> repo = new EFGenericRepository<WarehouseShelfs>();
        BaseService _serv = new BaseService();

        [HttpGet]
        public IEnumerable<WarehouseShelfs> Get()
        {
            return repo.GetList();
        }

        [HttpGet("{id}")]
        public WarehouseShelfs Get(int id)
        {
            return repo.GetItemById(id);
        }

        [HttpPost]
        public void Post([FromBody] ShelfDTO val)
        {
            repo.Create(new WarehouseShelfs()
            {
                Title = val.Title,
                Description = val.Description,
                IsDeleted = false,
                Xaxis = val.Xaxis,
                Yaxis  = val.Yaxis,
                Zaxis = val.Zaxis,
                CupboardId = val.CupboardId,
                TypeId = val.TypeId
            });
        }

        [HttpPut("{id}")]
        public void Put([FromBody] ShelfDTO val, int id)
        {
            var item = _serv._cont.WarehouseShelfs.Find(id);
            if(item != null)
            {
                item.Title = val.Title;
                item.Description = val.Description;
                item.IsDeleted = val.IsDeleted ?? false;
                item.Xaxis = val.Xaxis;
                item.Yaxis = val.Yaxis;
                item.Zaxis = val.Zaxis;
                item.CupboardId = val.CupboardId;
                item.TypeId = val.TypeId;

                repo.Update(item);
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repo.Remove(id);
        }
    }
}
