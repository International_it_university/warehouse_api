﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Serivce.Services.Product
{
    public class Product2DTO
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public double Xaxis { get; set; }
        public double Yaxis { get; set; }
        public double Zaxis { get; set; }
        public string Path { get; set; }
    }
}
