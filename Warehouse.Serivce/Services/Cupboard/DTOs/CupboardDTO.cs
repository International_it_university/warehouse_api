﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Serivce.Services.Cupboard.DTOs
{
    public class CupboardDTO: BaseDTO
    {
        public string Description { get; set; }
        public float Yaxis { get; set; }
        public float Zaxis { get; set; }
        public int WayId { get; set; }
    }
}
