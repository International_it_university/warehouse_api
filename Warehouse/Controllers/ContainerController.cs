﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Models;
using Warehouse.Models.WarehouseModels;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContainerController : BaseController
    {
        EFGenericRepository<TblContainer> _repo = new EFGenericRepository<TblContainer>(dbCntx);

        [HttpGet]
        public IEnumerable<Object> Get()
        {
            return _repo.GetList();
        }

        // GET: api/method/5
        [HttpGet("{id}")]//, Name = "Get")]
        public Object Get(int id)
        {
            return _repo.GetItemById(id);
        }

        // POST: api/method
        [HttpPost]
        public void Post([FromBody] ContainerModel value)
        {
            try
            {
                _repo.Create(new TblContainer
                {
                    Name = value.Name,
                    Description = value.Description,
                    WhId = value.WhID
                });
            }
            catch (Exception ex)
            {
                throw new Exception("ошибка при сохранении: " + ex.Message);
            }
        }

        // PUT: api/method/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ContainerModel value)
        {
            try
            {
                var item = _repo.GetItemById(id);
                item.Name = value.Name;
                item.Description = value.Description;
                item.WhId = value.WhID;

                _repo.Update(item);
            }
            catch (Exception ex) { throw new Exception("ошибка при обновлении: " + ex.Message); }
        }

        // DELETE: api/method/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                var item = _repo.GetItemById(id);
                if (item == null) throw new Exception("удаляемый элемент не найден");

                _repo.Remove(item);
            }
            catch (Exception ex) { throw new Exception("ошибка при удалении: " + ex.Message); }
        }
    }
}
