﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Warehouse.Serivce.db.EntityFramework
{
    public partial class iituwarehousedbContext : DbContext
    {
        public iituwarehousedbContext()
        {
        }

        public iituwarehousedbContext(DbContextOptions<iituwarehousedbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<WarehouseCupboards> WarehouseCupboards { get; set; }
        public virtual DbSet<WarehouseProducts> WarehouseProducts { get; set; }
        public virtual DbSet<WarehouseShelfs> WarehouseShelfs { get; set; }
        public virtual DbSet<WarehouseTypes> WarehouseTypes { get; set; }
        public virtual DbSet<WarehouseWays> WarehouseWays { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=iituwarehousedb.database.windows.net;Database=iituwarehousedb;User Id=adm;Password=Asdfasdf123;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WarehouseCupboards>(entity =>
            {
                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.Way)
                    .WithMany(p => p.WarehouseCupboards)
                    .HasForeignKey(d => d.WayId)
                    .HasConstraintName("FK_Cupboards_Ways");
            });

            modelBuilder.Entity<WarehouseProducts>(entity =>
            {
                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.QrCode).IsRequired();

                entity.Property(e => e.QrCodePath).IsRequired();

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Shelf)
                    .WithMany(p => p.WarehouseProducts)
                    .HasForeignKey(d => d.ShelfId)
                    .HasConstraintName("FK_Products_Shelfs");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.WarehouseProducts)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WarehouseProducts_WarehouseTypes");
            });

            modelBuilder.Entity<WarehouseShelfs>(entity =>
            {
                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.Cupboard)
                    .WithMany(p => p.WarehouseShelfs)
                    .HasForeignKey(d => d.CupboardId)
                    .HasConstraintName("FK_Shelfs_Cupboards");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.WarehouseShelfs)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WarehouseShelfs_WarehouseTypes");
            });

            modelBuilder.Entity<WarehouseTypes>(entity =>
            {
                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<WarehouseWays>(entity =>
            {
                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
