﻿using System;
using System.Collections.Generic;

namespace Warehouse.Serivce.db.EntityFramework
{
    public partial class WarehouseWays
    {
        public WarehouseWays()
        {
            WarehouseCupboards = new HashSet<WarehouseCupboards>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<WarehouseCupboards> WarehouseCupboards { get; set; }
    }
}
