﻿using Microsoft.EntityFrameworkCore;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text;
using Warehouse.Serivce.db.EntityFramework;

namespace Warehouse.Serivce.Services.Product
{
    public class ProductService
    {
        DbSet<WarehouseProducts> _dbSet;
        iituwarehousedbContext _context = new iituwarehousedbContext();
         
        public ProductService()
        {
            _dbSet = _context.Set<WarehouseProducts>();
        }

        public void Create(string path, string message)//Product2DTO obj)
        {
            //var item = new WarehouseProducts()
            //{
            //    Title = obj.Title,
            //    Description = obj.Description,
            //    Xaxis = obj.Xaxis,
            //    Yaxis = obj.Yaxis,
            //    Zaxis = obj.Zaxis
            //};
            //_dbSet.Add(item);
            //_context.SaveChanges();


            //string code = message; // Guid.NewGuid().ToString();// "test info";
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(message, QRCodeGenerator.ECCLevel.Q);
            //System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            //imgBarCode.Height = 150;
            //imgBarCode.Width = 150;
            // var xxx = qrCode.GetGraphic(20);

            using (Bitmap bitMap = qrCode.GetGraphic(20))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                    img.Save(path + "\\" + Guid.NewGuid().ToString() +".Jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    //imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                }
                //plBarCode.Controls.Add(imgBarCode);
            }
        }
    }
}
