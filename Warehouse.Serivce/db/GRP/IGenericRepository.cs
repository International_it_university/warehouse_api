﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Serivce.db.EntityFramework.GRP
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void Create(TEntity item);
        TEntity GetItemById(int id);
        IEnumerable<TEntity> GetList();
        IEnumerable<TEntity> GetList(Func<TEntity, bool> predicate);
        void Remove(TEntity item);
        void Update(TEntity item);
    }
}
