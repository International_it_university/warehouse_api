﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Serivce.Services.Product.DTOs
{
    public class ProductDTOs: BaseDTO
    {
        public string Description { get; set; }
        public float Xaxis { get; set; }
        public float Yaxis { get; set; }
        public float Zaxis { get; set; }
        public int TypeId { get; set; }
    }
}
