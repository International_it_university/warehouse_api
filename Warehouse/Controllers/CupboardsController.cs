﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Serivce.db;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;
using Warehouse.Serivce.Services.Cupboard.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CupboardsController : ControllerBase
    {
        EFGenericRepository<WarehouseCupboards> repo = new EFGenericRepository<WarehouseCupboards>();
        BaseService _serv = new BaseService();

        [HttpGet]
        public IEnumerable<WarehouseCupboards> Get()
        {
            return repo.GetList();
        }

        [HttpGet("{id}")]
        public WarehouseCupboards Get(int id)
        {
            return repo.GetItemById(id);
        }

        [HttpPost]
        public void Post([FromBody] CupboardDTO val)
        {
            //var way = _serv._cont.WarehouseWays.Find(val.WayId);
            var item = new WarehouseCupboards()
            {
                Id = 0,
                Title = val.Title,
                Description = val.Description,
                IsDeleted = false,
                Yaxis = val.Yaxis,
                Zaxis = val.Zaxis,
                WayId = val.WayId
            };

            repo.Create(item);
        }

        [HttpPut("{id}")]
        public void Put([FromBody] CupboardDTO val, int id)
        {
            var item = _serv._cont.WarehouseCupboards.Find(id);
            if (item != null)
            {
                item.Title = val.Title;
                item.Description = val.Description;
                item.IsDeleted = val.IsDeleted ?? false;
                item.ModifiedOn = DateTime.Now;
                item.Yaxis = val.Yaxis;
                item.Zaxis = val.Zaxis;
                item.WayId = val.WayId;

                repo.Update(item);
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repo.Remove(id);
        }
    }
}
