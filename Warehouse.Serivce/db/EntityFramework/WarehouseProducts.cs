﻿using System;
using System.Collections.Generic;

namespace Warehouse.Serivce.db.EntityFramework
{
    public partial class WarehouseProducts
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Xaxis { get; set; }
        public double Yaxis { get; set; }
        public double Zaxis { get; set; }
        public DateTime CreatedOn { get; set; }
        public string QrCodePath { get; set; }
        public int? ShelfId { get; set; }
        public string QrCode { get; set; }
        public int TypeId { get; set; }
        public bool IsDeleted { get; set; }

        public virtual WarehouseShelfs Shelf { get; set; }
        public virtual WarehouseTypes Type { get; set; }
    }
}
