﻿using System;
using System.Collections.Generic;

namespace Warehouse.Serivce.db.EntityFramework
{
    public partial class WarehouseTypes
    {
        public WarehouseTypes()
        {
            WarehouseProducts = new HashSet<WarehouseProducts>();
            WarehouseShelfs = new HashSet<WarehouseShelfs>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<WarehouseProducts> WarehouseProducts { get; set; }
        public virtual ICollection<WarehouseShelfs> WarehouseShelfs { get; set; }
    }
}
