﻿using System;
using System.Collections.Generic;

namespace Warehouse.Serivce.db.EntityFramework
{
    public partial class WarehouseShelfs
    {
        public WarehouseShelfs()
        {
            WarehouseProducts = new HashSet<WarehouseProducts>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public double Xaxis { get; set; }
        public double Yaxis { get; set; }
        public double Zaxis { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? CupboardId { get; set; }
        public int TypeId { get; set; }

        public virtual WarehouseCupboards Cupboard { get; set; }
        public virtual WarehouseTypes Type { get; set; }
        public virtual ICollection<WarehouseProducts> WarehouseProducts { get; set; }
    }
}
