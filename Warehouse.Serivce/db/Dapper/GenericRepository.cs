﻿using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace Warehouse.Serivce.db.Dapper
{
    public interface IRepository<T>
    {
        void Create(T item);
        void Delete(int id);
        T Get(int id);
        List<T> GetList();
        void Update(T item);
    }

    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private string _tblName = null;
        private string _connStr = null;

        public GenericRepository(string connectionString)
        {
            _connStr = connectionString;
        }

        public void SetTableName(string tblName)
        {
            _tblName = tblName;
        }

        public void Create(T item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public T Get(int id)
        {
            throw new NotImplementedException();
        }

        public List<T> GetList()
        {
            using (IDbConnection db = new SqlConnection(_connStr))
            {
                return db.Query<T>(String.Format("SELECT {0} FROM Users", _tblName)).ToList<T>();
            }
        }

        public void Update(T item)
        {
            throw new NotImplementedException();
        }
    }
}
