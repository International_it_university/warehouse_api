﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models.WarehouseModels
{
    public class BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
