﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models.WarehouseModels
{
    public class ShelfModel 
    {
        public int TypeID { get; set; }
        public int ContainerID { get; set; }
        public double Xaxis { get; set; }
        public double Yaxis { get; set; }
        public double Zaxis { get; set; }
    }
}
