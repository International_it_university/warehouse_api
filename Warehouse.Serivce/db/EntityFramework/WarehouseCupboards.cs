﻿using System;
using System.Collections.Generic;

namespace Warehouse.Serivce.db.EntityFramework
{
    public partial class WarehouseCupboards
    {
        public WarehouseCupboards()
        {
            WarehouseShelfs = new HashSet<WarehouseShelfs>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public double Yaxis { get; set; }
        public double Zaxis { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? WayId { get; set; }

        public virtual WarehouseWays Way { get; set; }
        public virtual ICollection<WarehouseShelfs> WarehouseShelfs { get; set; }
    }
}
