﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Serivce.db;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;
using Warehouse.Serivce.Services.Type.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypesController : ControllerBase
    {
        EFGenericRepository<WarehouseTypes> repo = new EFGenericRepository<WarehouseTypes>();
        BaseService _serv = new BaseService();
        [HttpGet]
        public IEnumerable<WarehouseTypes> Get()
        {
            return repo.GetList();
        }

        [HttpGet("{id}")]
        public WarehouseTypes Get(int id)
        {
            return repo.GetItemById(id);
        }

        [HttpPost]
        public void Post([FromBody] TypeDTO val)
        {
            repo.Create(new WarehouseTypes()
            {
                Title = val.Title,
                Description = val.Description,
                IsDeleted = false
            });
        }

        [HttpPut("{id}")]
        public void Put([FromBody] TypeDTO val, int id)
        {
            var item = _serv._cont.WarehouseTypes.Find(id);
            if(item != null)
            {
                item.Title = val.Title;
                item.Description = val.Description;
                item.IsDeleted = val.IsDeleted ?? false;
                item.ModifiedOn = DateTime.Now;

                repo.Update(item);
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repo.Remove(id);
        }
    }
}
