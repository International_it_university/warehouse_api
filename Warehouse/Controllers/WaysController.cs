﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Serivce.db;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;
using Warehouse.Serivce.Services.Way.DTOs;

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WaysController : ControllerBase
    {
        EFGenericRepository<WarehouseWays> repo = new EFGenericRepository<WarehouseWays>();
        BaseService _serv = new BaseService();
        
        [HttpGet]
        public IEnumerable<WarehouseWays> Get()
        {
            return repo.GetList();
        }

        [HttpGet("{id}")]
        public WarehouseWays Get(int id)
        {
            return repo.GetItemById(id);
        }

        [HttpPost]
        public void Post([FromBody] WayDTO val)
        {
            repo.Create(new WarehouseWays()
            {
                Title = val.Title,
                IsDeleted = false
            });
        }

        [HttpPut("{id}")]
        public void Put([FromBody] WayDTO val, int id)
        {
            var item = _serv._cont.WarehouseWays.Find(id);
            if(item != null)
            {
                item.Title = val.Title;
                item.IsDeleted = val.IsDeleted ?? false;
                item.ModifiedOn = DateTime.Now;

                repo.Update(item);
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repo.Remove(id);
        }
    }
}
