﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Serivce.Services
{
    public class BaseDTO
    {
        public string Title { get; set; }
        public bool? IsDeleted { get; set; } = false;
    }
}
