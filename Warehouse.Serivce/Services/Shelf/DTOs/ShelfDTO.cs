﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Serivce.Services.Shelf.DTOs
{
    public class ShelfDTO: BaseDTO
    {
        public string Description { get; set; }
        public float Xaxis { get; set; }
        public float Yaxis { get; set; }
        public float Zaxis { get; set; }
        public int CupboardId { get; set; }
        public int TypeId { get; set; }
    }
}