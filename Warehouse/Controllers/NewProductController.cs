﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Serivce.db;
using Warehouse.Serivce.db.EntityFramework;
using Warehouse.Serivce.db.EntityFramework.GRP;
using Warehouse.Serivce.Services.Product;
using Warehouse.Serivce.Services.Product.DTOs;
using Warehouse.Serivce.Services.Shelf.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewProductController : ControllerBase
    {
        ProductService _service = new ProductService();
        EFGenericRepository<WarehouseProducts> repo = new EFGenericRepository<WarehouseProducts>();
        BaseService _serv = new BaseService();

        IHostingEnvironment _host;
        public NewProductController(IHostingEnvironment host)
        {
            _host = host;
        }

        [HttpGet("TestMethod")]
        public string TestMethod()
        {
            string path = _host.ContentRootPath + "\\files";
            _service.Create(path, "Doszhan Baibatyrov TEST TEST TEST asfdasdf asdfasdfa sdf");

            var item = _host.ContentRootPath;
            var item2 = _host.WebRootPath;

            return item + "\\files" + "\\Test.Jpeg" + " ||| " + item2 ; // + " |||   " + item2;
        }

        [HttpGet]
        public IEnumerable<WarehouseProducts> Get()
        {
            return repo.GetList();
        }

        [HttpGet("{id}")]
        public WarehouseProducts Get(int id)
        {
            return repo.GetItemById(id);
        }

        [HttpPost]
        public void Post([FromBody] ProductDTOs val)
        {
            repo.Create(new WarehouseProducts()
            {
                Title = val.Title,
                Description = val.Description,
                Xaxis = val.Xaxis,
                Yaxis = val.Yaxis,
                Zaxis = val.Zaxis,
                IsDeleted = false,
                TypeId = val.TypeId,

            });
        }

        //// GET api/<NewProductController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    var item = _host.ContentRootPath;
        //    var item2 = _host.WebRootPath;

        //    return item +  "\\files" + item2;
        //}

        //[HttpPost]
        //public void Post([FromBody] ProductDTO item)
        //{
        //    item.Path = _host.ContentRootPath + "\\files";
        //    _service.Create(item);
        //}

        // PUT api/<NewProductController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<NewProductController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
